# Packaging CI/CD workflow

This repository contains the pipeline definition that handles the packaging workflow.
[`ci-package-builder.yml`](ci-package-builder.yml).

This pipeline handles the main packaging workflow:
1. converting the contents in git to source packages (`.dsc` and linked files)
2. uploading the source packages to OBS
3. monitor the build on OBS and relaying the build logs and results

It also deals with pulling updates from the upstream distribution:
1. import upstream packages to the matching git branch, for instance `debian/bullseye-security`
2. merge the changes into the appropriate branch, for instance `apertis/v2022-security`
3. scan the sources for inappropriate licenses, for instance complaining when
   trying to import GPL-3 files in a package to be landed in the `target` component

The [workflow guide](https://www.apertis.org/guides/component_guide/) documents
the most common operation.

To use this workflow:
* ensure that CI/CD is enabled on your packaging project
* set a few CI/CD variables at the project or group level:
  * `GITLAB_CI_USER`, `GITLAB_CI_PASSWORD`: credentials with push access to the
    git repositories to push tags and packaged sources on the `pristine-lfs-source` branch
  * `OSC_CONFIG`: settings and credentials to access OBS for ABI checks and lintian support
  * `OBS_SERVER`, `OBS_USER`, and `OBS_PASSWORD`: settings and credentials to build the package on OBS,
    further information available [here](https://gitlab.apertis.org/infrastructure/obs-gitlab-runner#required-environment)
* ensure your git repository follows the [expected structure](https://www.apertis.org/guides/component_structure/)
* set the configuration file path to
* `ci-package-builder.yml@infrastructure/ci-package-builder` in the CI/CD
  settings of your packaging project

## Where the pipeline is going to upload sources

The pipeline uses the details below to define on which OBS project the sources
are going to be uploaded:

* the `OBS_PREFIX` CI/CD variable, for instance `foo`
* the branch name, for instance `bar/baz-quux`, which get split in a branch prefix, `bar` and a suffix, `baz-quux`
* the component name from the `debian/apertis/component` file, for instance `frob`

With that, the pipeline will upload to the `foo:bar:baz:quux:frob` OBS project.

Customizing `bar` also requires to set the `OSNAME=bar` CI/CD variable.

> ⚠️ Warning!
>
> The GitLab issue [#35438](https://gitlab.com/gitlab-org/gitlab/-/issues/35438)
> "Regexp CI/CD variables are evaluated as strings" actually prevents this from
> working as intended and forking this repository is needed to customize
> the osname. The issue is expected to be fixed in GitLab 14.8.

## Extension points

The pipeline can be extended with child pipelines by adding the pipeline
definitions in your package:

* `debian/apertis/local-pre-source-build-gitlab-ci.yml` runs before attempting
  to build the source package from the content in git, providing a good place
  to run unit tests or other checks
* `debian/apertis/local-post-source-build-gitlab-ci.yml` runs after the source
  package has been built, before it gets uploaded to OBS, providing a good
  place to run [Lintian](https://wiki.debian.org/Lintian) checks

# Injecting an external APT repository to CI Build environment

The package builds are targetted against a particular release of the OS, making use of the specific versioned libraries and
dependencies shipped with the OS release.

There are scenarios where a user is required to pull in external dependencies.

* User wants to build a package, which has dependencies on other packages, that are not part of the OS release
* User wants to build a proprietary package, which is hosted in a separate private repository


The CI Build Environment provides a mechanism to inject external APT repository to cover the above mentioned use cases.

There are 3 environment flags that define this behavior.

* EXTRA_REPO_BASE_URL
* EXTRA_REPO_VERSION
* EXTRA_REPO_COMPONENTS

`EXTRA_REPO_BASE_URL` defines the APT repository url. It only supports standard http/s based urls, as well as http password
protected repository urls. Eg. `https://username:password@repositories.apertis.org/apertis/`
This variable is mandatory.

`EXTRA_REPO_VERSION` defines the APT repository release. An APT repository may host multiple releases, each distinguished by
its release names. An example release name is: `v2022`.
This variable is optional. If not specified, the release is determined from the running git branch.

`EXTRA_REPO_COMPONENTS` defines the APT repository components. Multiple repository components may be specified by space.
Eg. "target sdk development". This variable is mandatory.


  > ✍️ Note!
  >
  > If the mandatory options are not specified, the CI machinery will print a warning about it and disregard the extra APT repository


Below are some screenshots on how these variables and their values are fed into Gitlab CI

![Gitlab CICD](/imgs/env-variables-main.png "Gitlab CICD Overview")
![EXTRA_REPO_BASE_URL](/imgs/base-url.png "EXTRA_REPO_BASE_URL variable definition")
![EXTRA_REPO_VERSION](/imgs/repo-version.png "EXTRA_REPO_VERSION variable definition")
![EXTRA_REPO_COMPONENTS](/imgs/extra-components.png "EXTRA_REPO_COMPONENTS variable definition")

## Low-level manual operations

Some special cases that are not handled by the automation need to be dealt with
some manual operations, described below.

Most users can safely ignore this section.

### Manually Updating Components from Debian

Upstream updates are usually handled automatically by the
[`ci-package-builder.yml`](https://gitlab.apertis.org/infrastructure/ci-package-builder/)
Continuous Integration pipeline, which
[fetches upstream packages, merges them](https://www.apertis.org/guides/component_guide/#updating-components-from-debian)
with the Apertis contents and directly creates Merge Requests to be reviewed by
[maintainers](https://www.apertis.org/policies/contributions/#the-role-of-maintainers).

> ⚠️ Warning!
>
> The
[automated pipeline](https://www.apertis.org/guides/component_guide/#updating-components-from-debian)
should be used to perform this task where possible.
>
> This guide uses tools that are only provided in the package-source-builder
image. The easiest way to perform this task is to utilise the docker container.

However, in some cases it may be necessary to manually perform these
operations.

- Check out the source repository

      $ GITLAB_GROUP=pkg
      $ PACKAGE=hello
      $ git clone git@gitlab.apertis.org:${GITLAB_GROUP}/${PACKAGE}
      $ cd ${PACKAGE}

  > ✍️ Note!
  >
  > When handling packages in your personal GitLab namespace, `GITLAB_GROUP`
  should point to that, which is your GitLab username, rather than the main
  Apertis application group `pkg`.

- Ensure we have the required `debian/` and `upstream/` branches locally

      $ CURRENT_UPSTREAM=buster
      $ git branch debian/${CURRENT_UPSTREAM} origin/debian/${CURRENT_UPSTREAM}
      $ git branch upstream/${CURRENT_UPSTREAM} origin/upstream/${CURRENT_UPSTREAM}

- Ensure we have a `pristine-lfs` branch locally:

      $ git branch pristine-lfs origin/pristine-lfs

- Pull in the latest updates Debian from using the `apertis-pkg-pull-updates` script

      $ APERTIS_RELEASE=v2021
      $ MIRROR=https://deb.debian.org/debian
      $ git checkout -b apertis/${APERTIS_RELEASE} origin/apertis/${APERTIS_RELEASE}
      $ apertis-pkg-pull-updates --package=${PACKAGE} --upstream=${CURRENT_UPSTREAM} --mirror=${MIRROR}

  This will update the `debian/...`, `upstream/...` and `pristine-lfs` branches
  as prepared previously.

- Ensure that the changes to these branches are pushed to your remote `origin`.
  It is important that these branches are pushed and in sync with the default
  remote.

      $ git push origin --follow-tags pristine-lfs debian/${CURRENT_UPSTREAM} upstream/${CURRENT_UPSTREAM}

- The upstream changes now need to be merged into the apertis branch. We will
  do this via a development branch that can be pushed for review.

      $ PROPOSED_BRANCH=wip/${GITLAB_GROUP}/${APERTIS_RELEASE}-update
      $ git checkout -b ${PROPOSED_BRANCH} apertis/${APERTIS_RELEASE}
      $ apertis-pkg-merge-updates --upstream=debian/${CURRENT_UPSTREAM} --downstream=${PROPOSED_BRANCH}

- If the merge fails, fix the conflicts and continue

      $ git add ...
      $ git merge --continue

- Create a new changelog entry for the new version

      $ DEBEMAIL="User Name <user@example.com>" dch -D apertis --local +apertis --no-auto-nmu ""

- Check `debian/changelog` and update it if needed by listing all the remaining
  Apertis changes then commit the changelog.

      $ DEBIAN_VERSION=$(dpkg-parsechangelog -S Version)
      $ git commit -s -m "Release ${PACKAGE} version ${DEBIAN_VERSION}" debian/changelog

- Push your changes for review

      $ git push origin ${PROPOSED_BRANCH}:${PROPOSED_BRANCH}

  Follow the instructions provided by the above command to create a merge request

### Manually updating components from a newer Debian release

There are circumstances, when we deviate from the default upstream. This
usually happens when:

- Packages are not available in the default distribution repository
- Packages in the default distribution repository are outdated
- Newer version of package, available in the non-default repository, is needed

For example, Apertis v2021 ships with a newer version of the Linux kernel
(5.10.x) than Debian Buster (4.9.x) on which it is based, which is the kernel
found in the next Debian release called Bullseye. In such cases, special care
needs to be taken to update packages from their respective upstreams.

> ⚠️ Warning!
>
> The
[automated pipeline](https://www.apertis.org/guides/component_guide/#updating-components-from-debian)
should be used to perform this task where possible.
>
> This guide uses tools that are only provided in the package-source-builder
image. The easiest way to perform this task is to utilise the docker container.

Below are a set of steps which can be adapted to such exception packages. Let
us assume that for such repository, the package was picked from Debian Bullseye
instead of Buster

- Clone the repository:

      $ GITLAB_GROUP=pkg
      $ PACKAGE=hello
      $ git clone git@gitlab.apertis.org:${GITLAB_GROUP}/${PACKAGE}
      $ cd ${PACKAGE}

- If the required `debian/` and `upstream/` branches don't yet exist in your
  repository, create them based on the version that you have for instance. For
  instance, if you currently have a version of Apertis based on Debian Buster
  and wish to use the updated package in Bullseye:

      $ CURRENT_UPSTREAM=buster
      $ DESIRED_UPSTREAM=bullseye
      $ git push origin origin/upstream/${CURRENT_UPSTREAM}:refs/heads/upstream/${DESIRED_UPSTREAM}
      $ git push origin origin/debian/${CURRENT_UPSTREAM}:refs/heads/debian/${DESIRED_UPSTREAM}

- Ensure you have the required branches locally:

      $ DESIRED_UPSTREAM=bullseye
      $ git branch debian/${DESIRED_UPSTREAM} origin/debian/${DESIRED_UPSTREAM}
      $ git branch upstream/${DESIRED_UPSTREAM} origin/upstream/${DESIRED_UPSTREAM}

- Ensure we have a `pristine-lfs` branch locally:

      $ git branch pristine-lfs origin/pristine-lfs

- Pull in new updates using the `apertis-pkg-pull-updates` script, passing in
  the deviated upstream:

      $ APERTIS_RELEASE=v2021
      $ MIRROR=https://deb.debian.org/debian
      $ git checkout -b apertis/${APERTIS_RELEASE} origin/apertis/${APERTIS_RELEASE}
      $ apertis-pkg-pull-updates --package ${PACKAGE} --upstream ${DESIRED_UPSTREAM} --mirror ${MIRROR}

  This will update the `debian/...` and `upstream/...` branch as prepared
  previously.

- Ensure that the changes to these branches are pushed to your remote `origin`.
  It is important that these branches are pushed and in sync with the default
  remote.

      $ git push origin --follow-tags pristine-lfs debian/${DESIRED_UPSTREAM} upstream/${DESIRED_UPSTREAM}

- The upstream changes now need to be merged into the apertis branch. We will
  do this via a development branch that can be pushed for review.

      $ PROPOSED_BRANCH=wip/${GITLAB_GROUP}/${APERTIS_RELEASE}-update-from-${DESIRED_UPSTREAM}
      $ git checkout -b ${PROPOSED_BRANCH} apertis/${APERTIS_RELEASE}
      $ apertis-pkg-merge-updates --upstream=debian/${DESIRED_UPSTREAM} --downstream=${PROPOSED_BRANCH}

- If the merge fails, fix the conflicts and continue

      $ git add ...
      $ git merge --continue

- Create a new changelog entry for the new version

      $ DEBEMAIL="User Name <user@example.com>" dch -D apertis --local +apertis --no-auto-nmu ""

- Check `debian/changelog` and update it if needed by listing all the remaining
  Apertis changes then commit the changelog.

      $ DEBIAN_VERSION=$(dpkg-parsechangelog -S Version)
      $ git commit -s -m "Release ${PACKAGE} version ${DEBIAN_VERSION}" debian/changelog

- Push your changes for review

      $ git push origin ${PROPOSED_BRANCH}:${PROPOSED_BRANCH}

  Follow the instructions provided by the above command to create a merge request

# Enable lintian job for a specific package

To enable the lintian job for a specific package, you just need
to create a `debian/apertis/lintian` file in your package repository.

      $ touch debian/apertis/lintian
      $ git add debian/apertis/lintian
      $ git commmit -m "Add debian/apertis/lintian to enable lintian for my package"

The `ci-package-builder` will check the presence of this file and will enable
the lintian job if this file is present.

# Test pipeline and Credentials

To ensure the pipeline defined in `ci-package-builder.yml` works, we run
`run-dash-packaging-pipelines` that will use `ci-package-builder.yml` against
the `test/dash` repo under various scenarios. This job requires to define the
variable `TEST_GITLAB_AUTH_TOKEN`. This variable is a
[Project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)
from the `testbot` user.
The same token is used to defined the variable `GITLAB_CI_PASSWORD` in the
group `tests`.
