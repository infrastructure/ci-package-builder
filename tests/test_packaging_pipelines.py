#!/usr/bin/env python3

import argparse
import difflib
import logging
import os
import pathlib
import re
import shutil
import subprocess
import tempfile
import time
import urllib.parse
import zipfile
from urllib.error import HTTPError

import git
import gitlab
import gitlab.v4.objects
import osc.conf
import osc.core
import tenacity

SCRATCH_REPO_DIR = "/tmp/scratchrepo"
TEST_BRANCH = "wip/test/fake"
CONFIGURE_FAILURE_MESSAGE = "Configure failure."
LOCAL_PIPELINE_YAML = """
hello:
  stage: test
  tags: [ lightweight ]
  image: $DOCKER_IMAGE
  script:
    - echo This is a local pipeline
    - ls
    - env --null | sort -z | tr '\\0' '\\n'
  needs:
    - pipeline: $PARENT_PIPELINE_ID
      job: prepare-build-env
      artifacts: true
  rules:
    - when: on_success
"""


def _debian_mangle_version_to_tag(v):
    # see https://dep-team.pages.debian.net/deps/dep14/
    v = v.replace(":", "%")
    v = v.replace("~", "_")
    v = re.sub("\\.(?=\\.|$|lock$)", ".#", v)
    return v


def _get_branch_prefix(project):
    sep = "/"
    prefix = project.default_branch.split(sep)[0]
    return prefix + sep


def _retrying_on_exception(exception_type):
    retrying = tenacity.Retrying(
        stop=tenacity.stop_after_delay(120),
        retry=tenacity.retry_if_exception_type(exception_type),
        wait=tenacity.wait_fixed(10),
    )
    return retrying


def _find_stable_release_branch(project, prefix):
    branches = (b.name for b in project.branches.list(all=True))
    releases = [b for b in branches if b.startswith(prefix)]
    stable = (b for b in releases if b[-4:-1] != "dev" and b[-3:] != "pre")
    # do not pick the latest stable as it may not be actually released yet
    # for instance, in 2021Q1 we have the apertis/v2021 branch but it
    # should not be considered frozen yet since it has not been released
    second_latest = sorted(stable, reverse=True)[1]
    return second_latest


def _monitor_pipeline_for_completion(pipeline):
    logging.info(f"monitoring pipeline {pipeline.web_url}")
    while pipeline.status in ("running", "pending", "created", "waiting_for_resource"):
        logging.debug(f"pipeline {pipeline.web_url} status is {pipeline.status}")
        pipeline.refresh()
        time.sleep(5)


def _job_artifacts_namelist(job):
    with tempfile.TemporaryFile() as artifacts_file:
        job.artifacts(streamed=True, action=artifacts_file.write)
        artifacts_file.seek(0)
        with zipfile.ZipFile(artifacts_file) as artifacts_zip:
            artifacts = set(artifacts_zip.namelist())
            return artifacts


def _commit(gl, repo, msg):
    actor = git.Actor(gl.user.name, gl.user.email)
    commit = repo.index.commit(msg, author=actor, committer=actor)
    logging.info(f"committed {commit.hexsha} to {TEST_BRANCH}")
    return commit


def _push_strict(repo, *args, **kwargs):
    pushes = repo.remotes.origin.push(*args, **kwargs)
    for p in pushes:
        if p.flags & git.remote.PushInfo.ERROR:
            logging.error(
                f"failed to push ref {p.local_ref or ''}:{p.remote_ref} {p.summary.strip()}"
            )
        else:
            logging.debug(
                f"pushed {p.local_ref or ''}:{p.remote_ref} {p.summary.strip()}"
            )
    assert not any(p.flags & git.remote.PushInfo.ERROR for p in pushes)
    return pushes


class GitLabToOBSTester:
    def __init__(self, reference, scratch, testing_pipeline_url):
        self.apiurl = None
        self.gl = None
        self.reference = reference
        self.scratch = scratch
        self.scratch_git = None
        self.scratch_gitlab = None
        self.stable_branch = None
        self.testing_pipeline_url = testing_pipeline_url

    def connect(self, gitlab_instance, gitlab_server_url, gitlab_auth_token, oscrc):
        if gitlab_server_url:
            logging.info(f'Connecting to the "{gitlab_server_url}" instance')
            self.gl = gitlab.Gitlab(gitlab_server_url, private_token=gitlab_auth_token)
        else:
            logging.info(f'Connecting to the "{gitlab_instance}" configured instance')
            self.gl = gitlab.Gitlab.from_config(gitlab_instance)
        self.gl.auth()
        logging.info(f"Logged to GitLab as {self.gl.user.name} <{self.gl.user.email}>")
        osc.conf.get_config(override_conffile=oscrc)
        self.apiurl = osc.conf.config["apiurl"]

    def _scratch_set_properties(self):
        logging.info(f"setting up properties on {self.scratch}, temporarily disable CI")
        s = self.gl.projects.get(self.scratch)
        self.scratch_gitlab = s
        try:
            s.branches.delete(TEST_BRANCH)
        except gitlab.GitlabDeleteError:
            pass
        s.only_allow_merge_if_pipeline_succeeds = True
        s.merge_requests_access_level = "enabled"
        s.merge_method = "ff"
        s.builds_access_level = "disabled"
        s.lfs_enabled = True
        s.save()

    def _url_add_gitlab_auth(self, orig_url):
        url = urllib.parse.urlparse(orig_url)
        password = urllib.parse.quote(self.gl.private_token, safe="")
        netloc = f"oauth2:{password}@{url.netloc}"
        url = url._replace(netloc=netloc)
        return url.geturl()

    def _scratch_clone(self):
        url = self._url_add_gitlab_auth(self.scratch_gitlab.http_url_to_repo)
        if os.path.isdir(SCRATCH_REPO_DIR):
            logging.debug(f"Deleting previous {SCRATCH_REPO_DIR}")
            shutil.rmtree(SCRATCH_REPO_DIR)
        self.scratch_git = git.Repo.clone_from(url, SCRATCH_REPO_DIR)

    def prepare_scratch(self):
        self._scratch_set_properties()
        self._scratch_clone()

    def _scratch_set_variable(self, name, value):
        if not value:
            logging.info(f"deleting CI/CD variable '{name}'")
            try:
                self.scratch_gitlab.variables.delete(name)
            except gitlab.GitlabDeleteError:
                logging.warning(f"Variable '{name}' was not deleted")
            return
        logging.info(f"setting CI/CD variable '{name}' to '{value}'")
        vardata = {"key": name, "value": value}
        try:
            self.scratch_gitlab.variables.update(name, vardata)
        except gitlab.GitlabUpdateError:
            self.scratch_gitlab.variables.create(vardata)

    def _scratch_cleanup_tags(self, tags=None):
        scratch = self.scratch_git
        tags = tags if tags else list(scratch.tags)
        logging.debug(f"cleaning up the remote tags {','.join(t.name for t in tags)}")
        for tag in tags:
            try:
                # process tags one by one, so we can ignore failures due to
                # trying to delete from the scratch repository tags that are
                # only available in the reference one
                _push_strict(scratch, tag, delete=True, force=True, verbose=True)
            except git.exc.GitCommandError as e:
                if "remote ref does not exist" not in e.stderr:
                    raise
        scratch.delete_tag(tags)

    def _scratch_synchronize_branches(self, reference):
        scratch = self.scratch_git
        default = reference.default_branch
        logging.debug("synchronizing branches")
        prefix = _get_branch_prefix(reference)
        self.stable_branch = _find_stable_release_branch(reference, prefix)
        branches = [
            default,
            self.stable_branch,
            "pristine-lfs",
            "pristine-lfs-source",
            "debian/buster",
            "debian/bullseye",
        ]
        logging.info(f"synchronizing the {', '.join(branches)} branches")
        for branchname in branches:
            scratch.create_head(
                branchname,
                commit=scratch.remotes.reference.refs[branchname],
                force=True,
            )
        logging.debug(
            f"pushing {branches} to {self.scratch_gitlab.path_with_namespace}"
        )
        git = self.scratch_git.git
        git.lfs("fetch", "reference", "pristine-lfs-source", "--all")
        git.lfs("push", "origin", "pristine-lfs-source", "--all")
        _push_strict(scratch, branches, force=True, verbose=True, follow_tags=True)

    def reset_scratch(self):
        scratch = self.scratch_git
        logging.info(
            f"resetting {self.scratch_gitlab.path_with_namespace} to {self.reference}"
        )
        for b in self.scratch_gitlab.branches.list(all=True):
            if b.name.startswith("proposed-updates/"):
                b.delete()
        self._scratch_cleanup_tags()
        reference = self.gl.projects.get(self.reference)
        default = reference.default_branch
        url = self._url_add_gitlab_auth(reference.http_url_to_repo)
        scratch.create_remote("reference", url)
        scratch.remotes.reference.fetch()
        head = scratch.remotes.reference.refs[default]
        logging.debug(f"switching to the {TEST_BRANCH} branch as {head.commit.hexsha}")
        scratch.create_head(TEST_BRANCH, commit=head.commit).checkout()
        self._scratch_synchronize_branches(reference)
        logging.debug(
            f"setting the default branch on {self.scratch_gitlab.path_with_namespace} to {default} and re-enable CI"
        )
        self.scratch_gitlab.default_branch = default
        self.scratch_gitlab.builds_access_level = "enabled"
        self.scratch_gitlab.save()
        self._scratch_set_variable("OBS_PREFIX", self._get_obs_test_project_prefix())
        mrs = self.scratch_gitlab.mergerequests.list(state="opened")
        for mr in mrs:
            mr.state_event = "close"
            mr.save()

    def _get_obs_reference_project_name(self):
        root = self.scratch_gitlab.default_branch.replace("/", ":")
        component = "development"
        filepath = pathlib.Path(SCRATCH_REPO_DIR, "debian/apertis/component")
        if filepath.exists():
            component = open(filepath).read().strip()
        project = f"{root}:{component}"
        return project

    def _get_obs_test_project_prefix(self):
        username = osc.conf.config["api_host_options"][self.apiurl]["user"]
        test_project = f"home:{username}:branches:test:"
        return test_project

    def _get_obs_test_project_name(self):
        prefix = self._get_obs_test_project_prefix()
        reference_project = self._get_obs_reference_project_name()
        test_project = prefix + reference_project
        return test_project

    def _get_obs_branched_test_project_name(self):
        test_project = self._get_obs_test_project_name()
        git_branch_suffix = TEST_BRANCH.replace("/", "-")
        return f"{test_project}:{git_branch_suffix}"

    def obs_prepare_work_areas(self):
        logging.info("preparing work areas on OBS")
        package = self.reference.split("/")[-1]
        src_project = self._get_obs_reference_project_name()
        target_project = self._get_obs_test_project_name()
        msg = "Branch for testing the GitLab-to-OBS pipeline"
        logging.info(f"branching {package} from {src_project} to {target_project}")
        osc.core.branch_pkg(
            self.apiurl,
            src_project,
            package,
            target_project=target_project,
            msg=msg,
            force=True,
            add_repositories_block="never",
            add_repositories_rebuild="local",
        )

    def point_gitlab_ci_here(self, ci_config_path):
        logging.info(f"pointing to the CI definition at {ci_config_path}")
        self.scratch_gitlab.ci_config_path = ci_config_path
        self.scratch_gitlab.save()

    def _mr_wait(self, mr):
        for attempt in _retrying_on_exception(AssertionError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipelines = [
                    p
                    for p in mr.pipelines.list()
                    if p.sha == mr.sha and p.ref != mr.source_branch
                ]
                assert (
                    len(pipelines) == 1
                ), f"unexpected number of MR pipelines for commit {mr.sha} in branch {mr.source_branch}: {list(p['id'] for p in pipelines)}"
        mr_pipeline = self.scratch_gitlab.pipelines.get(pipelines[0].id)
        _monitor_pipeline_for_completion(mr_pipeline)
        return mr_pipeline

    def _get_source_package_name(self):
        cmd = subprocess.run(
            ["dpkg-parsechangelog", "-SSource"],
            capture_output=True,
            check=True,
            cwd=SCRATCH_REPO_DIR,
        )
        return cmd.stdout.decode().strip()

    def _branched_obs_package_exists(self):
        branched_project = self._get_obs_branched_test_project_name()
        source_package = self._get_source_package_name()

        logging.info("checking for OBS package %s/%s", branched_project, source_package)

        for attempt in _retrying_on_exception(HTTPError):
            # handle transient OBS errors
            with attempt:
                try:
                    osc.core.meta_exists(
                        "pkg", (branched_project, source_package), create_new=False
                    )
                except HTTPError as ex:
                    if ex.code == 404:
                        return False
                    raise
                else:
                    return True

    def create_release_commit(self):
        logging.info("creating a release commit")
        local_pipeline_yaml = "debian/apertis/local-gitlab-ci.yml"
        path_local_pipeline_yaml = pathlib.Path(SCRATCH_REPO_DIR) / local_pipeline_yaml
        path_local_pipeline_yaml.write_text(LOCAL_PIPELINE_YAML.strip())
        msg = f"Adding a local pipeline in {local_pipeline_yaml}"
        self.scratch_git.index.add(str(local_pipeline_yaml))
        _commit(self.gl, self.scratch_git, msg)
        subprocess.run(
            [
                "dch",
                "--increment",
                "--vendor=Apertis",
                "--distribution=apertis",
                "Fake changes while testing the GitLab-to-OBS pipeline",
            ],
            check=True,
            cwd=SCRATCH_REPO_DIR,
            env=dict(os.environ, DEBEMAIL="test@example.com"),
        )
        msg = f"Test release commit\n\nPipeline: {self.testing_pipeline_url}"
        self.scratch_git.index.add("debian/changelog")
        return _commit(self.gl, self.scratch_git, msg)

    def create_broken_commit(self):
        logging.info("creating a broken commit")

        configure_ac = pathlib.Path(SCRATCH_REPO_DIR) / "configure.ac"
        with configure_ac.open() as fp:
            orig_configure = fp.readlines()
            new_configure = [
                *orig_configure,
                f"AC_MSG_FAILURE([{CONFIGURE_FAILURE_MESSAGE}])",
            ]

        diff = "".join(
            difflib.unified_diff(
                orig_configure,
                new_configure,
                fromfile=f"a/{configure_ac.name}",
                tofile=f"b/{configure_ac.name}",
            )
        )
        diff += "\n"

        patches_dir = pathlib.Path(SCRATCH_REPO_DIR) / "debian" / "patches"
        patches_dir.mkdir(parents=True, exist_ok=True)

        diff_path = patches_dir / "configure-fail.diff"
        diff_path.write_text(diff)
        self.scratch_git.index.add(str(diff_path))

        series_path = patches_dir / "series"
        with series_path.open("a") as fp:
            print(diff_path.name, file=fp)
        self.scratch_git.index.add(str(series_path))

        msg = f"Test broken commit\n\nPipeline: {self.testing_pipeline_url}"
        return _commit(self.gl, self.scratch_git, msg)

    def test_broken_commit(self, commit):
        logging.info("submitting a broken commit")
        _push_strict(self.scratch_git, TEST_BRANCH, force=True, verbose=True)
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=TEST_BRANCH, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "failed"
        ), f"submitted release pipeline {pipeline.web_url} should have failed: '{pipeline.status}'"
        jobs = {j.name: j for j in pipeline.jobs.list()}
        job_names = set(jobs)
        assert job_names == {
            "prepare-build-env",
            "scan-licenses",
            "build-source",
            "upload",
            "obs-capture",
            "cleanup",
        }, f"submitted release pipeline on unmerged changes expected different jobs: {job_names}"
        assert (
            self._branched_obs_package_exists()
        ), "OBS branched project does not exist"
        bridges = {b.name: b for b in pipeline.bridges.list()}
        bridge_names = set(bridges.keys())
        assert bridge_names == {
            "local-pipeline-pre-source-build-compat",
            "obs",
        }, f"unexpected bridge jobs: {bridge_names}"
        obs_bridge = bridges["obs"]
        assert (
            obs_bridge.status == "failed"
        ), f"submitted release pipeline {pipeline.web_url}'s OBS bridge should have failed: '{obs_bridge.status}'"
        obs_pipeline = self.scratch_gitlab.pipelines.get(
            obs_bridge.downstream_pipeline["id"]
        )
        for job_info in obs_pipeline.jobs.list():
            assert job_info.name.startswith(
                "obs-"
            ), f"unexpected job name: {job_info.name}"

            job = self.scratch_gitlab.jobs.get(job_info.get_id())
            trace = job.trace().decode()
            assert "Build failed" in trace, f"expected build failure in trace:\n{trace}"
            assert (
                CONFIGURE_FAILURE_MESSAGE in trace
            ), f"expected failed build logs in trace:\n{trace}"

            artifacts = _job_artifacts_namelist(job)
            assert (
                "build.log" in artifacts
            ), f"build job {job.web_url} is missing artifact 'build.log': {artifacts}"

    def reset_to_release_commit(self, commit):
        logging.info("resetting to release commit")
        self.scratch_git.head.reset(commit.hexsha, index=True, working_tree=True)

    def test_release_commit(self, commit):
        logging.info("submitting a release commit")
        _push_strict(self.scratch_git, TEST_BRANCH, force=True, verbose=True)
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=TEST_BRANCH, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"submitted release pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "prepare-build-env",
            "scan-licenses",
            "build-source",
            "upload",
            "obs-capture",
            "cleanup",
        }, f"submitted release pipeline on unmerged changes expected different jobs: {job_names}"
        assert (
            self._branched_obs_package_exists()
        ), "OBS branched project does not exist"
        bridges = {b.name: b for b in pipeline.bridges.list()}
        bridge_names = set(bridges.keys())
        assert bridge_names == {
            "local-pipeline-pre-source-build-compat",
            "obs",
        }, f"unexpected bridge jobs: {bridge_names}"
        obs_pipeline = self.scratch_gitlab.pipelines.get(
            bridges["obs"].downstream_pipeline["id"]
        )
        for job_info in obs_pipeline.jobs.list():
            assert job_info.name.startswith(
                "obs-"
            ), f"unexpected job name: {job_info.name}"

            job = self.scratch_gitlab.jobs.get(job_info.get_id())
            artifacts = _job_artifacts_namelist(job)
            assert any(
                f.startswith("results/") and f.endswith(".deb") for f in artifacts
            ), f"build job {job.web_url} is missing artifact 'results/*.deb': {artifacts}"
        return commit

    def test_release_mr_to_frozen_branch(self, commit):
        logging.info("testing MR to frozen branch")
        target = self.stable_branch
        mr = self.scratch_gitlab.mergerequests.create(
            dict(
                source_branch=TEST_BRANCH,
                target_branch=target,
                title="Test release on frozen branch",
            )
        )
        logging.info(f"created release MR {mr.web_url} to frozen branch {target}")
        mr_pipeline = self._mr_wait(mr)
        assert (
            mr_pipeline.status == "failed"
        ), f"release MR pipeline {mr_pipeline.web_url} to frozen branch should have failed: '{mr_pipeline.status}'"
        job_names = {j.name for j in mr_pipeline.jobs.list()}
        # Do not include "scan-licenses" since this is tested against stable branch, which at this point is v2021
        # which does not support it
        assert job_names == {
            "freeze-stable-branches",
            "prepare-build-env",
            "scan-licenses",
            "build-source",
            "upload",
            "obs-capture",
            "cleanup",
        }, f"submitted non-release pipeline expected different jobs: {job_names}"
        mr.state_event = "close"
        mr.save()
        logging.info("MR on frozen branch blocked successfully ✅")

    def test_release_mr_to_default_branch(self, commit):
        logging.info("testing MR to default branch")
        target = self.scratch_gitlab.default_branch
        mr = self.scratch_gitlab.mergerequests.create(
            dict(source_branch=TEST_BRANCH, target_branch=target, title="Test release")
        )
        logging.info(f"created release MR {mr.web_url} to default branch {target}")
        mr_pipeline = self._mr_wait(mr)
        assert (
            mr_pipeline.status == "success"
        ), f"mr pipeline {mr_pipeline.web_url} didn't succeed: {mr_pipeline.status}"
        assert (
            self._branched_obs_package_exists()
        ), "OBS branched project does not exist"
        logging.info(f"landing {mr.web_url}")
        for attempt in _retrying_on_exception(gitlab.exceptions.GitlabMRClosedError):
            # sometimes GitLab need some time to compute the MR status correctly
            with attempt:
                mr.merge()
        for attempt in _retrying_on_exception(IndexError):
            # give some time to GitLab to kick the pipeline when it detects new commits
            with attempt:
                pipeline = self.scratch_gitlab.pipelines.list(
                    ref=self.scratch_gitlab.default_branch, sha=commit.hexsha
                )[0]
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"landed release pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        job_names = {j.name for j in pipeline.jobs.list()}
        assert job_names == {
            "prepare-build-env",
            "scan-licenses",
            "build-source",
            "upload",
            "obs-capture",
        }, f"landed release pipeline on merged changes expected different jobs: {job_names}"
        # Temporarily commented out due to:
        # https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114
        # assert (
        #     not self._branched_obs_package_exists()
        # ), "OBS branched project still exists after merge completion"
        logging.info("Release MR landed successfully ✅")

    def test_release_artifacts(self, commit):
        cmd = subprocess.run(
            ["dpkg-parsechangelog", "-SVersion"],
            capture_output=True,
            check=True,
            cwd=SCRATCH_REPO_DIR,
        )
        version = cmd.stdout.decode().strip()
        mangledversion = _debian_mangle_version_to_tag(version)
        prefix = self.scratch_gitlab.default_branch.split("/")[0]
        tagname = f"{prefix}/{mangledversion}"
        self.scratch_git.remotes.origin.fetch(tags=True)
        tag = self.scratch_git.tags[tagname]
        assert (
            tag.commit == commit
        ), f"tag {tagname} points to {tag.commit}, expected {commit}"
        project = self._get_obs_test_project_name()
        package = self.reference.split("/")[-1]
        files = osc.core.meta_get_filelist(self.apiurl, project, package)
        dscs = [f for f in files if f.endswith(".dsc")]
        assert len(dscs) == 1, f"expected a single .dsc, got {dscs}"
        dsc = dscs[0]
        expected = f"{package}_{version}.dsc"
        assert dsc == expected, f"expected '{expected}' on OBS, got '{dsc}'"
        logging.info(f"Found {tagname} and {dsc} succesfully ✅")

    def test_release_retrigger_pipeline(self, commit, rebuild=True):
        branch = self.scratch_gitlab.default_branch
        logging.info(
            f"triggering again a pipeline on the {branch} release branch should have no ill effects ({rebuild=})"
        )
        project = self._get_obs_test_project_name()
        package = self.reference.split("/")[-1]
        repository = "default"
        architecture = "x86_64"
        last_build = osc.core.get_buildhistory(
            self.apiurl, project, package, repository, architecture, limit=1
        )
        params = {"ref": branch}
        if not rebuild:
            params["variables"] = [{"key": "OBS_REBUILD_DISABLED", "value": "1"}]
        pipeline = self.scratch_gitlab.pipelines.create(params)
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"the retriggering pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        this_build = osc.core.get_buildhistory(
            self.apiurl, project, package, repository, architecture, limit=1
        )
        if rebuild:
            assert (
                this_build != last_build
            ), "no new builds got triggered even if OBS_REBUILD_DISABLED=1 was not set"
        else:
            assert (
                this_build == last_build
            ), "a new build got triggered even if OBS_REBUILD_DISABLED=1 was set"
        logging.info(f"Retriggering pipelines does not fail ({rebuild=}) ✅")

    def test_upstream_pull_up_to_date(self):
        branch = "debian/buster"
        logging.info(
            f"triggering a pipeline on the already up-to-date {branch} upstream branch should succeed"
        )
        pipeline = self.scratch_gitlab.pipelines.create({"ref": branch})
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"the retriggering pipeline {pipeline.web_url} didn't succeed: '{pipeline.status}'"
        logging.info("Upstream pull on up-to-date branch succeeded ✅")

    def test_downstream_pull_mirror(self):
        downstream_osname = "downstream"
        source_known_tag = "apertis/0.5.10.2-5co2"
        source_branch = self.scratch_gitlab.default_branch
        release = source_branch.split("/", 1)[-1]
        downstream_branch = downstream_osname + "/" + release
        logging.info(f"testing mirroring from {source_branch} to {downstream_branch}")
        scratch = self.scratch_git
        logging.info(f"checking out the old known tag {source_known_tag}")
        source_known_tagged_commit = scratch.tags[source_known_tag].commit
        scratch.git.checkout()
        logging.info(f"rolling back {source_branch} to {source_known_tag}")
        scratch.create_head(
            source_branch, commit=source_known_tagged_commit, force=True
        )
        scratch.create_head(
            downstream_branch, commit=source_known_tagged_commit, force=True
        )
        logging.info(
            f"roll back branches and tags on {self.scratch_gitlab.path_with_namespace}"
        )
        newer_tags = [tag for tag in scratch.tags if tag.name > source_known_tag]
        self._scratch_cleanup_tags(newer_tags)
        _push_strict(
            scratch,
            [source_branch, downstream_branch],
            force=True,
            verbose=True,
            push_option="ci.skip",
        )
        logging.info(f"trigger mirroring pipeline on {source_branch}")
        pipeline = self.scratch_gitlab.pipelines.create(
            {
                "ref": source_branch,
                "variables": [
                    {"key": "OSNAME", "value": downstream_osname},
                    {
                        "key": "OSNAME_BRANCH_MATCH",
                        "value": f"/^{downstream_osname}\\//",
                    },
                    {
                        "key": "MIRROR_BRANCH_MATCH",
                        "value": "/^apertis\\//",
                    },
                    {
                        "key": "DOCKER_IMAGE",
                        "value": f"$CI_REGISTRY/infrastructure/apertis-docker-images/{release}-package-source-builder",
                    },
                    {
                        "key": "DEBIAN_MIRROR",
                    },
                    {
                        "key": "MIRROR_APT_REPOSITORY_PREFIX",
                        "value": "https://repositories.apertis.org/apertis/dists",
                    },
                    {
                        "key": "MIRROR_GIT_REPOSITORY_PREFIX",
                        "value": "https://gitlab.apertis.org/pkg",
                    },
                    {"key": "DOWNSTREAM_RELEASES", "value": downstream_branch},
                ],
            }
        )
        _monitor_pipeline_for_completion(pipeline)
        assert (
            pipeline.status == "success"
        ), f"mirroring pipeline {pipeline.web_url} didn't succeed: {pipeline.status}"
        scratch.remotes.origin.fetch(tags=True)
        new_commits = list(
            scratch.iter_commits(f"{source_branch}..origin/{source_branch}")
        )
        assert new_commits, "the pull-mirror pipeline didn't import any new commit"
        new_tags = [tag.name for tag in scratch.tags if tag.name > source_known_tag]
        assert new_commits, "the pull-mirror pipeline didn't import any new commit"
        assert new_tags, "the pull-mirror pipeline didn't import any new tag"
        known_tag = "apertis/0.5.11+git20200708+dd9ef66-5apertis0"
        assert (
            known_tag in new_tags
        ), f"the expected {known_tag} was not in the imported tags: {new_tags}"
        mrs = [
            mr
            for mr in self.scratch_gitlab.mergerequests.list(state="opened")
            if mr.target_branch == downstream_branch
        ]
        assert (
            mrs
        ), f"the pull-mirror pipeline didn't create any MR targeting {downstream_branch} from {source_branch}"
        assert (
            len(mrs) == 1
        ), f"too many MRs after the pull-mirror pipeline run: {[mr.web_url for mr in mrs]}"
        mr = mrs[0]
        mr_pipeline = self._mr_wait(mr)
        assert (
            mr_pipeline.status == "success"
        ), f"mr pipeline {mr_pipeline.web_url} didn't succeed: {mr_pipeline.status}"
        logging.info(f"merging MR {mr.web_url}")
        for attempt in _retrying_on_exception(gitlab.exceptions.GitlabMRClosedError):
            # sometimes GitLab need some time to compute the MR status correctly
            with attempt:
                mr.merge()
        logging.info("Mirroring for downstreams succeeded ✅")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test the GitLab-to-OBS pipeline")
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--scratch-repository",
        type=str,
        required=True,
        help="the repository on which the pipeline is tested",
    )
    parser.add_argument(
        "--reference-repository",
        type=str,
        required=True,
        help="the scratch repository will be reset to the reference-repository contents",
    )
    parser.add_argument(
        "--gitlab-instance",
        type=str,
        default="apertis",
        help="get connection parameters from this configured instance",
    )
    parser.add_argument(
        "--gitlab-auth-token", type=str, help="the GitLab authentication token"
    )
    parser.add_argument("--gitlab-server-url", type=str, help="the GitLab instance URL")
    parser.add_argument(
        "--oscrc", type=str, help="the OSC configuration with the OBS credentials"
    )
    parser.add_argument(
        "--testing-pipeline-url",
        type=str,
        help="The URL of the pipeline running the test",
    )
    parser.add_argument(
        "--ci-config-path",
        type=str,
        required=True,
        help="The path to the CI config to test",
    )
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel or logging.INFO)

    t = GitLabToOBSTester(
        reference=args.reference_repository,
        scratch=args.scratch_repository,
        testing_pipeline_url=args.testing_pipeline_url,
    )
    t.connect(
        args.gitlab_instance, args.gitlab_server_url, args.gitlab_auth_token, args.oscrc
    )
    t.prepare_scratch()
    t.reset_scratch()
    t.obs_prepare_work_areas()
    t.point_gitlab_ci_here(args.ci_config_path)
    release_commit = t.create_release_commit()
    broken_commit = t.create_broken_commit()
    t.test_broken_commit(broken_commit)
    t.reset_to_release_commit(release_commit)
    t.test_release_commit(release_commit)
    t.test_release_mr_to_frozen_branch(release_commit)
    t.test_release_mr_to_default_branch(release_commit)
    t.test_release_artifacts(release_commit)
    t.test_release_retrigger_pipeline(release_commit)
    t.test_release_retrigger_pipeline(release_commit, rebuild=False)
    t.test_upstream_pull_up_to_date()
    t.test_downstream_pull_mirror()
    logging.info("Test completed successfully ✨")
